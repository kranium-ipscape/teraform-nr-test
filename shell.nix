{ nixpkgs ? import <nixpkgs> {} }:

let
  terraform-provider-newrelic = nixpkgs.callPackage ./pkgs/terraform-provider-newrelic { };
in
{
  ipscapeTerraformEnv = nixpkgs.stdenv.mkDerivation {
    name = "ipscape-terraform-env";
    buildInputs = with nixpkgs; [
      (terraform.withPlugins (old: [terraform-provider-newrelic]))
    ];
  };
}
