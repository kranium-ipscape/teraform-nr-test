# variable "newrelic_api_key" {}

provider "newrelic" {
#  api_key = "${var.newrelic_api_key}"
  api_key = "<ADD YOUR KEY HERE>"
}

# Create an alert policy
resource "newrelic_alert_policy" "demo_alert" {
  name = "Alert"
}

# Add a notification channel
resource "newrelic_alert_channel" "demo_email" {
  name = "email"
  type = "email"

  configuration = {
    recipients              = "kranium@ipscape.com.au"
    include_json_attachment = "1"
  }
}

# Link the channel to the policy
resource "newrelic_alert_policy_channel" "alert_email" {
  policy_id  = "${newrelic_alert_policy.demo_alert.id}"
  channel_id = "${newrelic_alert_channel.demo_email.id}"
}

resource "newrelic_infra_alert_condition" "demo_foo" {
  policy_id = "${newrelic_alert_policy.demo_alert.id}"

  name       = "High disk usage"
  type       = "infra_metric"
  event      = "StorageSample"
  select     = "diskUsedPercent"


comparison = "above"

  critical {
    duration      = 25
    value         = 90
    time_function = "all"
  }
}
