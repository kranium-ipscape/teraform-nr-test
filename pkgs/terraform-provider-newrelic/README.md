steps to generate nix files:

go get -u github.com/paul91/terraform-provider-newrelic
cd $GOPATH/src/github.com/paul91/terraform-provider-newrelic
git checkout -b feature/newrelic-infra-conditions origin/feature/newrelic-infra-conditions
cd ..
mv paul91 terraform
cd newrelic-infra-conditions
go2nix save
